<?php
/**
 * Academy Pro.
 *
 * This file adds the pricing page template to the Academy Pro Theme.
 *
 * Template Name: Product Category
 *
 * @package Academy
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    https://my.studiopress.com/themes/academy/
 */

add_filter( 'body_class', 'academy_add_body_class' );
/**
 * Adds the pricing page body class.
 *
 * @since 1.0.0
 *
 * @param array $classes Current list of classes.
 * @return array New classes.
 */
function academy_add_body_class( $classes ) {

	$classes[] = 'products-page';
	return $classes;

}
add_action( 'genesis_entry_content', 'source1_products_list', 10 );

function source1_products_list() {
	if ( have_rows( 'products_list' ) ) : ?>
<div class="products-list">
	<?php
		while ( have_rows( 'products_list' ) ) :
the_row();
?>
	<div class="product">
		<a href="<?php the_sub_field( 'link_location' ); ?>">
			<img src="<?php the_sub_field( 'image' ); ?>">
			<span class="link-button"><span class="triangle-ocn">&#x25b6;</span>

				<?php the_sub_field( 'link_text' ); ?></span>
		</a>
	</div>

	<?php endwhile; ?>
</div>
<?php
endif;
}

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

// Removes footer cta.
remove_action( 'genesis_before_footer', 'academy_footer_cta' );

// Runs the Genesis loop.
genesis();